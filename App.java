import java.util.LinkedList;
import javafx.scene.paint.Color;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Polygon;

public class App extends Application {

    private Stage stage;
    private TextField sizeField;

    @Override
    public void start(Stage primaryStage) {
        this.stage = primaryStage;

        createInitialContent();

        primaryStage.setTitle("Home");
        primaryStage.show();
    }

    private void createInitialContent() {
        VBox root = new VBox(10);
        root.setAlignment(Pos.CENTER);

        Label heading = new Label("LinkedList");

        heading
                .setStyle("-fx-font-size: 80px;-fx-text-fill: black;-fx-font-style: italic;-fx-font-family:Roboto;");

        Label text = new Label("Enter size of Linkedlist");
        text.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        text.setStyle("-fx-font-size: 18px; -fx-text-fill: green; -fx-font-weight: bold;");

        sizeField = new TextField();
        sizeField.setPromptText("Enter an integer");
        sizeField.setMaxWidth(150);

        Button button = new Button("Go to Next");
        button.setStyle("-fx-font-size: 14px; -fx-background-color: #4CAF50; -fx-text-fill: white;");
        button.setMinWidth(80);

        button.setOnAction(e -> {
            createNextContent(); // Pass the text to the next content creation method
        });

        root.getChildren().addAll(heading, text, sizeField, button);

        Scene scene = new Scene(root, 300, 250);
        stage.setScene(scene);
    }

    private LinkedList<Integer> linkedList;
    private TextField[] textFields;

    private void createNextContent() {
        int size = Integer.parseInt(sizeField.getText());
        createLinkedList(size);
    }
    //copied code here
      
    private void createLinkedList(int size) {
        linkedList = new LinkedList<>();
        textFields = new TextField[size];

        VBox root = new VBox(10);
        root.setAlignment(Pos.CENTER);

        for (int i = 0; i < size; i++) {
            textFields[i] = new TextField();
            root.getChildren().add(textFields[i]);
        }

        Button button = new Button("Store Elements");
         button.setStyle("-fx-font-size: 14px; -fx-background-color: #4CAF50; -fx-text-fill: white;");
        button.setMinWidth(80);
        button.setOnAction(e ->{ storeElements();
        customizeTextFieldWidth();
    });

        root.getChildren().add(button);

        Scene scene = new Scene(root, 300, 250);
        stage.setScene(scene);
    }

    private void customizeTextFieldWidth() {
        for (TextField textField : textFields) {
            textField.setPrefWidth(150); // Set the preferred width as needed
        }
    }

    private void storeElements() {
        for (TextField textField : textFields) {
            linkedList.add(Integer.parseInt(textField.getText()));
        }

        goToNextPage();
    }

    TextField txt[];

    private void goToNextPage() {
         VBox root = new VBox(10);
    root.setAlignment(Pos.CENTER);

    HBox hBox = new HBox(10);
    hBox.setAlignment(Pos.CENTER);

    for (int i = 0; i < linkedList.size(); i++) {
        StackPane stackPane = new StackPane();

        Circle circle = new Circle(30);
        circle.setFill(null);
        circle.setStroke(Color.BLACK);
        Text text = new Text(String.valueOf(linkedList.get(i)));

        stackPane.getChildren().addAll(circle,text);
        hBox.getChildren().add(stackPane);

        if (i < linkedList.size() - 1) {
            Polygon arrowhead = new Polygon();
            arrowhead.getPoints().addAll(0.0,0.0,10.0,5.0,0.0,10.0);
            Line line = new Line();
            line.setStartX(0.0f);
            line.setStartY(0.0f);
            line.setEndX(50.0f);
            line.setEndY(0.0f);

            hBox.getChildren().addAll(line,arrowhead);
        }
    }

    root.getChildren().add(hBox);

    Scene scene = new Scene(root, 600, 400);
    stage.setScene(scene);
}

    public static void main(String[] args) {
        launch(args);
    }
}
